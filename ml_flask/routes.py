import os
import sys

from ml_flask.ml_model import MLModel

from flask import Flask, request, jsonify

from ml_flask import app

@app.route('/predict', methods=["GET"])
def predict():
    test_data = request.args.get('data')
    ml_mod = MLModel()
    output = ml_mod.predict(app.config["MODEL_FILE"],
                    test_data)
    return jsonify(output)


if __name__== "__main__":
    app.run(port=5000, debug=True)