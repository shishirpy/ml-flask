# ML-Flask
Easiest deployment of a prediction function.

## Aim

Deploy machine learning models as `flask` APIs.

## Requirements

What will you need:

1. A trained model with `predict` method.
