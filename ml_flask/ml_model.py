import pickle
import os

class MLModel(object):
    def predict(self, model_path, data):
        """
        Parameters
        -------------
        model_path : str
            path of the pickled model

        data : numpy array
            numpy array of the sample that needs to be predicted.
        """
        print(model_path)
        print(os.getcwd())
        print("=="*20)
        with open(model_path, "rb") as f:
            model = pickle.load(f)
        print(data)
        print(os.getcwd())
        print("\n\n")
        print("=="*20)
        return model.predict(data)